<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
    public $timestamps = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'id', 'email');

    protected $fillable = array('name', 'email', 'password');
    protected $guarded = array('id');

    /**
     * Model-based data validation rules
     * @todo replace with Ardent-like package for auto-validate on save
     */
    public static function validate($input)
    {
        $validationRules = array(
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|alphanum|confirmed',
            'password_confirmation' => 'required|alphanum'
        );

        return Validator::make($input, $validationRules);
    }

    /**
     * Hash password before inserting
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function posts()
    {
        return $this->hasMany('Post', 'user_id');
    }

    public function comment()
    {
        return $this->hasMany('Comment', 'id');
    }
}

<?php

class Post extends Eloquent { 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

    protected $hidden = array('user_id', 'id');
    protected $fillable = array('title', 'content', 'user_id');
    protected $guarded = array('id');

    /**
     * Model-based validation rules
     * @todo replace with Ardent-like package for auto-validate on save
     */
    public static function validate($input)
    {
        $validationRules = array(
            'title' => 'required|max:100',
            'content' => 'required',
        );

        return Validator::make($input, $validationRules);
    }

    public function user()
    {
        return $this->belongsto('User', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }
}

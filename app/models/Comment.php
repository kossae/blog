<?php

class Comment extends Eloquent { 
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comments';

    protected $fillable = array('content', 'user_id');
    protected $guarded = array('id', 'user_id', 'post_id');

    /**
     * Model-based validation rules
     * @todo replace with Ardent-like package for auto-validate on save
     */
    public static function validate($input)
    {
        $validationRules = array(
            'content' => 'required',
        );

        return Validator::make($input, $validationRules);
    }

    public function user()
    {
        return $this->belongsto('User', 'user_id');
    }

    public function post()
    {
        return $this->belongsTo('Post', 'post_id');
    }
}

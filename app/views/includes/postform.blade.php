<div id="post" class="reveal-modal" data-reveal>
    <div class="row">
        <div class="medium-12 columns">
            <h3>New Blog Post</h3>
            {{-- Build Blog Post Form --}}
            {{ Form::open(array('url' => 'create')) }}
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title') }}
                {{ Form::label('content', 'Content (BBCode Supported)') }}
                {{ Form::textarea('content') }}
                {{ Form::submit('Post') }}
            {{ Form::close() }}
            {{-- End Blog Post Form --}}
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>

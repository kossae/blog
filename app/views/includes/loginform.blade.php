<div id="login" class="reveal-modal" data-reveal>
    <div class="row">
        <div class="medium-6 columns">
            <h3>Log In</h3>
            {{-- Build Login Form --}}
            {{ Form::open(array('url' => 'login')) }}
                {{ Form::label('login_email', 'Email') }}
                {{ Form::email('login_email') }}
                {{ Form::label('login_password', 'Password') }}
                {{ Form::password('login_password') }}
                {{ Form::submit('Login') }}
            {{ Form::close() }}
            {{-- End Login Form --}}
        </div>
        <div class="medium-6 columns">
            <h3>Create Account</h3>
            {{-- Build Account Form --}}
            {{ Form::open(array('url' => 'register')) }}
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name') }}
                {{ Form::label('email', 'Email') }}
                {{ Form::email('email') }}
                {{ Form::label('password', 'Password') }}
                {{ Form::password('password') }}
                {{ Form::label('password_confirmation', 'Confirm Password') }}
                {{ Form::password('password_confirmation') }}
                {{ Form::submit('Create Account') }}
            {{ Form::close() }}
            {{-- End Account Form --}}
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>

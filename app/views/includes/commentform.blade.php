<div class="row">
    <div class="medium-6 columns">
        @if (Auth::check())
            <h3>Post A Comment</h3>
            {{-- Build Comment Form --}}
            {{ Form::open(array('url' => 'postComment')) }}
                {{ Form::label('content', 'Comment') }}
                {{ Form::textarea('content') }}
                {{ Form::hidden('post_id', $post->id) }}
                {{ Form::submit('Submit Comment') }}
            {{ Form::close() }}
            {{-- End Comment Form --}}
        @else
            Please Log In to post a comment
        @endif
    </div>
</div>

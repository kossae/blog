<ul>
    <li>{{ HTML::linkAction('HomeController@index', 'All') }}</li>
    @foreach ($authors as $author)
        <li>{{ HTML::linkAction('HomeController@index', $author->name, array('user_id' => $author->id)) }}</li>    
    @endforeach
</ul>

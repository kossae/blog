<h4>Navigation</h4>
<ul id="nav">
    <li>{{ HTML::linkAction('HomeController@index', 'Home') }}</li>
</ul>

@if (Auth::check())
    <a href="#" data-reveal-id="post">New Post</a> <br/>
    {{ HTML::linkAction('AccountController@logout', 'Logout') }} 
@else
    Please <a href="#" data-reveal-id="login">log in or create an account</a>
@endif
<br />
{{ link_to('postFeed', 'Latest Posts (JSON)') }}

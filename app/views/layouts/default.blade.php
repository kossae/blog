<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="container">

    <div class="row">
        <div class="medium-12 columns">
            @include('includes.header')
        </div>
    </div>

    <div id="main" class="row">
        <div class="medium-9 push-3 columns page">
            @yield('content')
        </div>
        <div class="medium-3 pull-9 columns">
            @include('includes.sidebar')
        </div>
    </div>
    @if (Auth::check())
        @include('includes.postform')
    @else
        @include('includes.loginform')
    @endif
</div>
    @include('includes.extrajs')
</body>
</html>

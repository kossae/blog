@extends('layouts.default')
@section('content')
    @if ($errors->count() > 0)
        <ul id="errors">
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    @endif
    @if (Session::get('alert-success'))
        <ul id="success">
            <li>{{ Session::get('alert-success') }}</li>
        </ul>
    @endif
    <div class="row authorbar">
        <div class="medium-12 columns">
            @include('includes.authorbar')
        </div>
    </div>
    @foreach ($posts as $post)
        <div class="row">
            <div class="medium-12 columns post-title">
                <h4>{{ HTML::linkAction('PostController@view', $post->title, array('post_id' => $post->id)) }}</h4>
                @if(Auth::check() && (Auth::getUser()->id == $post->user->id))
                    <span class="delete-button">{{ HTML::linkAction('PostController@delete', 'Delete Post', array('post_id' => $post->id)) }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns post-author">
                <span>{{ $post->user->name }} </span>
            </div>
            <div class="medium-6 columns post-date">
                <span>{{ date("m/d/Y", strtotime($post->created_at)) }}</span>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns post-content">
                <p>{{ $post->content }}</p>
                <p>{{ HTML::linkAction('PostController@view', $post->comments->count().' comments', array('post_id' => $post->id)) }}
            </div>
        </div>
    @endforeach
    <div class="row pagination">
        <div class="medium-12 columns">
            {{ $posts->appends(Input::except('page'))->links() }}
        </div>
    </div>
@stop

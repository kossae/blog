@extends('layouts.default')
@section('content')
    @if ($errors->count() > 0)
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    @endif
    @if (Session::get('alert-success'))
        <ul id="success">
            <li>{{ Session::get('alert-success') }}</li>
        </ul>
    @endif
    <div class="row">
        <div class="medium-12 columns post-title">
            <h4>{{ $post->title }}</h4>
            @if(Auth::check() && (Auth::getUser()->id == $post->user->id))
                <span class="delete-button">{{ HTML::linkAction('PostController@delete', 'Delete post', array('post_id' => $post->id)) }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="medium-6 columns post-author">
            <span>{{ $post->user->name }} </span>
        </div>
        <div class="medium-6 columns post-date">
            <span>{{ date("m/d/Y", strtotime($post->created_at)) }}</span>
        </div>
    </div>
    <div class="row">
        <div class="medium-12 columns post-content">
            <p>{{ $post->content }}</p>
        </div>
    </div>
    <div class="row">
        <div class="medium-12 columns post-comments">
            @foreach ($comments as $comment)
                <div class="row comment">
                    <div class="medium-12 columns">
                        <p>{{ $comment->content }}</p>
                        <h5>{{ $comment->user->name }}</h5>
                        @if(Auth::check() && (Auth::getUser()->id == $comment->user->id))
                            <span class="delete-button">{{ HTML::linkAction('PostController@deleteComment', 'Delete comment', array('comment_id' => $comment->id)) }}</span>
                        @endif
                    </div>
                </div>
            @endforeach
            @include('includes.commentform')
        </div>
    </div>
@stop

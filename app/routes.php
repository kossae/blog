<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/**
 * GET routes
 */
Route::get('/', 'HomeController@index');
Route::get('logout', 'AccountController@logout');
Route::get('deletePost', 'PostController@delete');
Route::get('deleteComment', 'PostController@deleteComment');
Route::get('postFeed', function() {
    return Post::with('user')->take(10)->get();
});
Route::get('view', 'PostController@view');

/**
 * POST routes
 */
Route::post('login', 'AccountController@login');
Route::post('register', 'AccountController@register');
Route::post('create', 'PostController@create');
Route::post('postComment', 'PostController@postComment');

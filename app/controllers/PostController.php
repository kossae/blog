<?php

class PostController extends BaseController 
{
    /**
     * Create new post
     */
    public function create()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            $validate = Post::validate(Input::all());
            if ($validate->passes()) {
                $data = Input::only(array('title', 'content')); 

                // Set post user
                $data['user_id'] = $user->id;

                // Parse bbCode
                $data['content'] = BBCode::parse($data['content']);

                $post = Post::create($data);
                if ($post) {
                    return Redirect::to('/')->with('alert-success', 'Post created successfully!');
                } else {
                    return Redirect::back()->withErrors(array('Error creating post'));
                }
            } else {
                return Redirect::back()
                    ->withErrors($validate)
                    ->withInput(Input);
            }
        } else {
            return Redirect::back()->withErrors(array('You must be logged to post!'));
        }
    }

    public function delete()
    {
        $post = Post::find(Request::query('post_id'));
        if ($post) {
            if (Auth::check() && Auth::getUser()->id == $post->user_id) {
                $post->delete();
                return Redirect::to('/')->with('alert-success', 'Post has been deleted');
            } else {
                return Redirect::back()->withErrors('You are not allowed to delete this post');
            }
        } else {
            return Redirect::back()->withErrors(array('Cannot find post to delete'));
        }

    }

    public function view()
    {
        $post = Post::find(Request::query('post_id'));

        if ($post) {
            $comments = $post->comments()->orderBy('id', 'desc')->get();
            return View::make('pages.view')->with(array('post' => $post, 'comments' => $comments));
        } else {
            return Redirect::back();
        }
    }

    public function postComment()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            $post = Post::find(Input::get('post_id'));
            if ($post) {
                // manual assignment
                $comment = new Comment();
                $comment->user_id = $user->id;
                $comment->content = Input::get('content');
                $comment->post_id = Input::get('post_id');
                $comment->save();

                return Redirect::back()->with('alert-success', 'Comment posted successfully!');
            } else {
                return Redirect::back()->withErrors('Invalid post id');
            }
        } else {
            return Redirect::back()->withErrors(array('You are not authorized to post a comment'));
        }
    }

    public function deleteComment()
    {
        $comment = Comment::find(Request::query('comment_id'));
        if ($comment) {
            if (Auth::check() && Auth::getUser()->id == $comment->user_id) {
                $comment->delete();
                return Redirect::back()->with('alert-success', 'Comment has been deleted');
            } else {
                return Redirect::back()->withErrors('You are not allowed to delete this comment');
            }
        } else {
            return Redirect::back()->withErrors(array('Cannot find comment to delete'));
        }

    }
}

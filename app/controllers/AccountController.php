<?php

class AccountController extends BaseController 
{
    /**
     * Login action
     */
    public function login()
    {
        $validationRules = array(
            'login_email' => 'required|email',
            'login_password' => 'required|alphaNum'
        );  

        Log::info(Input::all());
        $validator = Validator::make(Input::all(), $validationRules);

        if ($validator->fails()) {
            return Redirect::to('/')
                ->withErrors($validator)
                ->withInput(Input::except('login_password'));
        } else {
            $data = array(
                'email' => Input::get('login_email'),
                'password' => Input::get('login_password')
            );

            if (Auth::attempt($data)) {
                return Redirect::back()->with('alert-success', 'You are now logged in');
            } else {
                return Redirect::back()->withErrors(array('Invalid email and/or password'));
            }
        }
    }

    /**
     * Logout action
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

    /**
     * Account registration action
     */
    public function register()
    {
        $validate = User::validate(Input::all());
        if ($validate->passes()) {
            $data = Input::only(array('name', 'email', 'password'));
            $data['password'] = Hash::make($data['password']);
            $user = User::create($data);
            if ($user) {
                Auth::login($user);
            }
            return Redirect::to('/')->with('alert-success', 'User account created');
        } else {
            return Redirect::to('/')
                ->withErrors($validate)
                ->withInput(Input::except('password'));
        }
    }
}

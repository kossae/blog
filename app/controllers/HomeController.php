<?php

class HomeController extends BaseController 
{

    /**
     * Index action
     */
	public function index()
	{
        // get posts
        if ($userId = Request::query('user_id')) {
            $posts = Post::where('user_id', '=', $userId)->orderBy('id', 'desc')->paginate(10);
        } else {
            $posts = Post::orderBy('id', 'desc')->paginate(10);
        }

        // get authors
        $authors = User::orderBy('name')->get();

		return View::make('pages.home')->with(array('posts' => $posts, 'authors' => $authors));
	}
}
